# tp3

## Objectif:
Mise en place d'une chaîne d'intégration continu, dans le cadre du déploiement d'une application en Python.


## Description des fichiers fournis:
- Fichiers Python : permettant de faire fonctionner l'application.
- Fichiers json : permettent de stocker et d'échanger des données.
- gitlab-CI.yml -> Pipeline de l'application contenant les différentes étapes à déployer (pour Gitlab, des stages).
  Elle contient les différentes étapes et les différents scripts d'exécution de nos actions.
- Dockerfile : fichier de configuration qui contient les différentes étapes à suivre permettant de construire une image Docker.
- Le dossier docker-test: ce dernier contient les différents outils que l'on veut tester, répartis dans des dossiers distincts.

- Le fichier docker-compose.yaml, contenu à la racine du dossier docker-test, permet de tester ses outils de CI en local sur sa machine à l'aide de la commande: docker compose up

  Ces derniers seront par la suite implémentés dans notre pipeline, une fois les tests réussit.

  Le fait de travailler en local permet de gagner un temps d'exécution important, au lieu de systématiquement déclencher une pipeline qui va mettre un certain temps avant de se déployer.

  Il peut néanmoins arriver que certaines choses fonctionnent en local, mais ne fonctionneront pas sur le cloud comme des restrictions d'accès au réseau ou à des ressources externes à notre machine locale.


### Description des pré-requis :
- Disposer d'un compte sur un outil d'orchestration (ici Gitlab), permettant de stocker du code et d'exécuter un pipeline.
- Disposer d'un compte Docker Hub permettant de pouvoir stocker et mettre à disposition si besoin, notre image préalablement construite.


## Schéma de présentation des étapes du pipeline
Le pipeline va contenir différentes étapes (stages) :
- clone: permettant de cloner un projet sur un repository Git, afin de l'utiliser dans notre pipeline.
- lint: le lint va effectuer la vérification des normes de codage avec pylint.
- test: l'étape test va effectuer la vérification des copier-collers dans le code avec radon-cc,
        faire une analyse cyclomatique avec radon-raw,
        lancer des tests unitaires avec unit test.
- code_metrics: cette étape va servir à produire des rapports afin de les rendre disponibles.
- buildandpush: va permettre d'envoyer l'image Docker sur le Docker Hub.

Chaque stage possède sa propre section de script pour définir les commandes qui doivent être exécutées.
- artefacts: pour spécifier les fichiers ou les répertoires à archiver.
- variables: est utilisé pour définir les variables d'environnement pour l'étape robot_tests.
- dependencies: l'étape buildandpush garantit qu'elle ne s'exécute qu'une fois que les étapes précédentes se soient terminées avec succès.


## Configuration des paramètres de pipeline
- Création des variables d'environnements sur le repository Gitlab dans les paramètres CI/CD:

    DOCKERHUB_PASSWORD: mot de passe d'accès au dépôt Dockerhub.

    DOCKERHUB_USERNAME: nom d'utilisateur du compte Dockerhub.

    ROBOT_PATH: chemin du dossier machine.robot.

    ROBOT_MACHINE_FILE: nom du fichier machine.robot à utiliser.

    UNITTEST_FILE: chemin vers le nom du fichier pour réaliser les tests unitaires.

    CI_COMMIT_SHORT_SHA: permetà GitLab CI de prendre automatiquement la valeur du commit en cours et de l'utiliser pour étiqueter et pousser l'image Docker correspondante.

Le fait de variabiliser un pipeline va permettre de rendre dynamique et personnalisable une succession d'étapes,
et offre plus de facilité en terme de gestion utilisateur.

Il est possible de variabiliser le déclenchement automatique de la pipeline au moment du commit en lui stipulant une action: auto, ou bien dans un script externe configurable et paramétrable.


